provider "vault" {
}

resource "vault_mount" "ssh-client" {
  path        = "ssh-client"
  type        = "ssh"
}

resource "vault_ssh_secret_backend_ca" "ssh-client" {
    backend = "${vault_mount.ssh-client.path}"
    generate_signing_key = true
}

resource "vault_ssh_secret_backend_role" "client-default" {
    name                    = "default"
    backend                 = "${vault_mount.ssh-client.path}"
    key_type                = "ca"
    allow_user_certificates = true
    default_extensions      = {
      "permit-pty": "",
      "permit-port-forwarding":"",
      "permit-X11-forwarding":"",
      "permit-agent-forwarding":""
      }
    default_user            = "root"
    allowed_users           = "root"
}

resource "vault_mount" "ssh-server" {
  path        = "ssh-server"
  type        = "ssh"
}

resource "vault_ssh_secret_backend_ca" "ssh-server" {
    backend = "${vault_mount.ssh-server.path}"
    generate_signing_key = true
}

resource "vault_ssh_secret_backend_role" "server-default" {
    name                    = "default"
    backend                 = "${vault_mount.ssh-server.path}"
    key_type                = "ca"
    allow_host_certificates = true
}
