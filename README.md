  Vault supports SSH backends that makes it easier to manage SSH certificates.
  This allows to make Client SSH carts, and Server SSH certs.
  The certs are signed by CA, so there is no personal data set on the Server side, and there is no per-server entry on the client side.

This kind of authorization is very useful when we want to make servers dynamic (like Cloud instances that are frequently recreated, added or removed)  and for dynamic set of users (like temporary access, revoking access and adding access for new users)
