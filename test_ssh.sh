#/usr/bin/env bash
set -ex

# Geneate cert
vault write -field=signed_key ssh-client/sign/default public_key=@$HOME/.ssh/id_rsa.pub valid_principals=root> $HOME/.ssh/id_rsa-cert

ssh-keygen -L -f $HOME/.ssh/id_rsa-cert
